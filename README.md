# php project flight
Infrastructure management for several high-traffic PHP applications


## Getting started
To configure a server cluster, I created the following command:
```
flight provision <CLUSTER_NAME>
```
## Add your files
```
cd existing_repo
git remote add origin https://gitlab.com/hieuhanufit/php-project-flight.git
git branch -M main
git push -uf origin main
```

## References:
https://themsaid.com/infrastructure-management-for-several-high-traffic-php-applications